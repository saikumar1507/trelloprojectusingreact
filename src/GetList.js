import React, { useState } from 'react'
import ListAdd from './ListAdd'
import { useEffect } from 'react'
import TrelloLists from './TrelloList'
let boardId = '623afa747f70097e37db6797'
let key = 'ca306ee3bb37dcc0aefd448047b05d63'
let token = 'ab25220c61d4bfadce681521aa57499eea73f31867f52b7227737eadbd7af39d'


let listsId = {}
const GetList = () => {
  const [item, setItem] = useState([])
  useEffect(() => {
    async function ListData() {
      let URL = await fetch('https://api.trello.com/1/boards/623afa747f70097e37db6797/lists?key=ca306ee3bb37dcc0aefd448047b05d63&token=ab25220c61d4bfadce681521aa57499eea73f31867f52b7227737eadbd7af39d', {
        method: 'GET'
      })
      return URL.json()
    }
    ListData().then((res) => {
      let ListsArray = []
      res.forEach(element => {
        ListsArray.push({ 'id': element.id, 'name': element.name })
        listsId[element.name] = element.id
      })
      setItem(ListsArray)
    })
  }, [])

  const deleteLists = (id) => {
    setItem((item.filter((lists) => lists.id != id)))
    
    fetch(`https://api.trello.com/1/lists/${id}/closed?value=true&key=${key}&token=${token}`, {
      method: 'PUT'
    })
      .then(response => {
        console.log(
          `Response: ${response.status} ${response.statusText}`
        );
        return response.text();
      })
      .then(text => console.log(text))
      .catch(err => console.error(err));
  }


  return (
    <>
      {item.map((data) => {
        return <TrelloLists name={data.name} id={data.id} temp={listsId} listArray={item} onSelect={deleteLists} />
      })}
    </>
  )
}
export default GetList
