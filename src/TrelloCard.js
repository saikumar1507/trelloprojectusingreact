import React, { useState } from 'react'
import './index.css'
import PopUp from './PopUp'
import ReactDOM from 'react-dom'
import 'tachyons'

const TrelloCard = (props) => {
  let cardId = props.cardId
  let listId = props.temp
  let key = 'ca306ee3bb37dcc0aefd448047b05d63'
  let token = 'ab25220c61d4bfadce681521aa57499eea73f31867f52b7227737eadbd7af39d'

  let card_id
  let listName


  const [popUp, setPopUp] = useState(false)

  let cards = document.querySelectorAll('.cards')
  let lists = document.querySelectorAll('.lists-Container')

  cards.forEach(item => {
    item.addEventListener('dragstart', () => {
      card_id = cardId[item.innerHTML]
      item.classList.add('dragging')
    })
    item.addEventListener('dragend', () => {
      listName = listId[item.parentElement.parentElement.firstChild.innerHTML]

      // UPDATING DRAG DROP IN TRELLO BOARD
      async function updatingDrag() {
        let response = await fetch(`https://api.trello.com/1/cards/${card_id}?pos=bottom&idList=${listName}&key=${key}&token=${token}`, {
          method: "PUT"
        })
        return response.json();
      }
      updatingDrag()
        .then(text => console.log(text))
      item.classList.remove('dragging')
    })
  })

  lists.forEach(list => {
    list.addEventListener('dragover', (e) => {
      e.preventDefault()
      const draggable = document.querySelector('.dragging')
      list.appendChild(draggable)
    })
  })
  

  return (

    <>
      <li draggable='true' className='cards grow' onClick={() => setPopUp(true)}>{props.name}</li>
      <PopUp showPop={popUp} closePop={() => setPopUp(false)} name={props.name} cardId={props.cardId} card_id={props.card_id} />
    </>
  )
}
export default TrelloCard