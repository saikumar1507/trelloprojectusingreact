import React, { useEffect, useState } from 'react'
import './index.css'
import CheckList from './CheckList'
import 'tachyons'
let key = 'ca306ee3bb37dcc0aefd448047b05d63'
let token = 'ab25220c61d4bfadce681521aa57499eea73f31867f52b7227737eadbd7af39d'
let object = {}
let checkLists = {}
const PopUp = (props) => {

    const [paragraph, setParagraph] = useState('')
    const [desc, setDesc] = useState('')
    const [textArea, setTextArea] = useState(true)
    const [comment, setComment] = useState('')
    const [addComment, setAddComment] = useState([])

    const [check, setCheck] = useState('')
    const [isCheck, setIsCheck] = useState(false)
    const [checkInfo, setCheckInfo] = useState([])

    // CARD ID
    let cardId = props.cardId

    const descriptionText = (event) => {
        setParagraph(event.target.value)
    }


    const saveDescription = () => {
        setDesc(paragraph)
        setTextArea(!textArea)

        // POSTING DESCRIPTION
        fetch(`https://api.trello.com/1/cards/${cardId[props.name]}?key=${key}&token=${token}&desc=${paragraph}`, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json'
            }
        })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
            .then(text => console.log(text))
            .catch(err => console.error(err));
    }

    //  GETTING DESCRIPTION
    useEffect(() => {
        async function getDescription() {
            let URL = await fetch(`https://api.trello.com/1/cards/${cardId[props.name]}?key=${key}&token=${token}`, {
                method: 'GET'
            })
            return URL.json()
        }

        getDescription().then((description) => {
            let descValue = description.desc
            setDesc(descValue)

        })

    }, [])


    const commentText = (event) => {
        setComment(event.target.value)
    }

    const saveComment = () => {
        setAddComment((prevComment) => {
            return [...prevComment, comment]
        })

        // POSTING COMMENT
        fetch(`https://api.trello.com/1/cards/${cardId[props.name]}/actions/comments?text=${comment}&key=${key}&token=${token}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json'
            }
        })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
            .then(text => console.log(text))
            .catch(err => console.error(err));
    }

    // GETTING COMMENT
    useEffect(() => {
        async function getComment() {
            let URL = await fetch(`https://api.trello.com/1/cards/${cardId[props.name]}/actions?key=${key}&token=${token}`, {
                method: 'GET',
            })
            return URL.json()
        }
        getComment().then((comment) => {
            let commentsArray = []
            comment.forEach((comm) => {
                commentsArray.push(comm.data.text)
            })
            setAddComment(commentsArray)
        })

    }, [])


    const checkListText = (event) => {
        setCheck(event.target.value)
    }

    const checkList = () => {
        setIsCheck(true)

    }
    const addCheckList = () => {
        setCheckInfo((oldCheck) => {
            return [...oldCheck, check]
        })
        // POST CHECK LIST
        let title = check
        let id = props.card_id
        object[title] = id


        //  POST CHECK LIST
        fetch(`https://api.trello.com/1/cards/${cardId[props.name]}/checklists?name=${title}&key=${key}&token=${token}`, {
            method: 'POST'
        })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
            .then(text => {
                let checkId = JSON.parse(text)
                checkLists[title] = checkId.id

            })
            .catch(err => console.error(err));


    }
    
    // GETTING CHECK LIST
    useEffect(() => {
        async function getCheckList() {
            let URL = await fetch(`https://api.trello.com/1/cards/${cardId[props.name]}/checklists?key=${key}&token=${token}`, {
                method: 'GET'
            })
            return URL.json()
        }
        getCheckList().then((check_List)=>{
            let checkListArray=[]
            check_List.forEach((check_Lists)=>{
                checkListArray.push(check_Lists.name)
                checkLists[check_Lists.name]=check_Lists.id
                
            })
            setCheckInfo(checkListArray)
        })
    }, [])

    const deleteCheckList = (id, val) => {
        setCheckInfo((check_list) => {
            return check_list.filter((element, index) => {

                return index != id
            })
        })

        fetch(`https://api.trello.com/1/checklists/${checkLists[val]}?key=${key}&token=${token}`, {
            method: 'DELETE'
        })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
            .then(text => console.log(text))
            .catch(err => console.error(err));
    }

    if (!props.showPop) return 

    return (
        <>
            <div className='overlay' />
            <div className='PopUp'>
                <form action="#">
                    <div className='title'>
                        <h3>{props.name}</h3>
                        <button onClick={props.closePop} className='closeBtn'>X</button>
                    </div>
                    <div className='Description'>
                        <h3>Description</h3>
                        <p>{desc}</p>
                        {
                            textArea ? <textarea id="textArea" cols="60" rows="5" placeholder='Add a more detailed description...' onChange={descriptionText} ></textarea> : null
                        }
                        {
                            textArea ? <button onClick={saveDescription} >SAVE</button> : <button onClick={saveDescription} >EDIT</button>
                        }
                        {
                            textArea ? <button className='closeDescription' onClick={() => setTextArea(!textArea)}>X</button> : null
                        }
                    </div>

                    {/* CHECK LIST */}
                    <div className='checkList'>
                        <div className='checkListEntry' onClick={checkList}>
                            <h3 className='check grow'><i class="fa-solid fa-check-to-slot"></i>checklist</h3>
                            {isCheck ? <input type="text" placeholder='Add a check list...' onChange={checkListText} className='checkListInput' /> : null}
                            {isCheck ? <button onClick={addCheckList} className='checkListAdd grow'>Add</button> : null}
                            <ol>
                                {checkInfo.map((checkListInfo, index) => {
                                    return <CheckList cardId={cardId} value={checkListInfo} id={index} onSelect={deleteCheckList} checkLists={checkLists} card_id={props.card_id} cardName={props.name} />
                                })}
                            </ol>
                        </div>
                    </div>

                    <div className='comment'>
                        <h3>Activity</h3>
                        <input type='text' placeholder='Write a comment...' onChange={commentText}></input>
                        <button onClick={saveComment} className='commentSaveBtn grow'>SAVE</button>

                        <ol>
                            {addComment.map((comment) => {
                                return <li className='commentText grow'>{comment}</li>
                            })}
                        </ol>
                    </div>
                </form>
            </div>
        </>
    )
}
export default PopUp