import './App.css';
import ListAdd from './ListAdd';

function App() {
  return (
    <div className="App">
        <div className='container'>
          <ListAdd />
        </div>
      </div>
  );
}

export default App;
