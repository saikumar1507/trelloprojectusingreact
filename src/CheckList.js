import React, { useContext, useEffect, useState } from 'react'
import './index.css'
import 'tachyons'
import CheckListItem from './CheckListItem'

let checkItemId = {}

let key = 'ca306ee3bb37dcc0aefd448047b05d63'
let token = 'ab25220c61d4bfadce681521aa57499eea73f31867f52b7227737eadbd7af39d'
const CheckList = (props) => {

    const [showInput, setShowInput] = useState(false)
    const [addCheck, setAddCheck] = useState('')
    const [checkInfo, setCheckInfo] = useState([])

    const AddItem = () => {
        !showInput ? setShowInput(true) : setShowInput(false)

    }

    // CARD ID
    const id = props.card_id
    
    // POST CHECK LIST ITEM
    let checkLists = props.checkLists

    const saveCheckList = () => {
        setCheckInfo((checkItem) => {
            return [...checkItem, addCheck]
        })

        // POST CHECK ITEM
        fetch(`https://api.trello.com/1/checklists/${checkLists[props.value]}/checkItems?name=${addCheck}&key=${key}&token=${token}`, {
            method: 'POST'
        })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
            .then(text => {
                let checkListItemsIds = (JSON.parse(text))
                checkItemId[addCheck] = checkListItemsIds['id']
            })
            .catch(err => console.error(err));


    }
       
    // GETTING CHECK ITEM
    useEffect(() => {
        async function getCheckItem() {
            let URL = await fetch(`https://api.trello.com/1/checklists/${checkLists[props.value]}/checkItems?key=${key}&token=${token}`, {
                method: 'GET'
            })
            return URL.json()
        }
        getCheckItem().then((checkItem)=>{
            let checkItemList=[]
            checkItem.forEach((item)=>{
                checkItemList.push(item.name)
                checkItemId[item.name]=item.id
            })
            setCheckInfo(checkItemList)
        })
        
    }, [])


    const checkText = (event) => {
        setAddCheck(event.target.value)
    }

    return (
        <div className='checkListContainer'>
            <div className='completeCheckList'>
                <h2>{props.value}</h2>
                <i className="fa-solid fa-trash deleteCheckList" onClick={() => { props.onSelect(props.id, props.value) }}></i>
            </div>
            <ol>
                {checkInfo.map((check) => {
                    return (<CheckListItem value={check} id={props.card_id} checkLists={checkLists} checkItemId={checkItemId} title={props.value} cardId={props.cardId} cardName={props.cardName}/>)
                })}
            </ol>
            <div>
                {showInput ? <input type='text' onChange={checkText} className='addCheckList'></input> : null}
                {showInput ? <button onClick={saveCheckList} className='save'>Save</button> : null}
                <h4 onClick={AddItem} className='addItem grow'>+ Add an item</h4>

            </div>
        </div>
    )
}
export default CheckList