import React, { useEffect, useState } from 'react'
import TrelloCard from './TrelloCard'
import 'tachyons'


let key = 'ca306ee3bb37dcc0aefd448047b05d63'
let token = 'ab25220c61d4bfadce681521aa57499eea73f31867f52b7227737eadbd7af39d'

let cardId = {}

const TrelloLists = (props) => {

    const [card, setCard] = useState({ id: "", name: "" })
    const [cardItem, setCardItem] = useState([])
    const [card_id, setCardId] = useState('')

    

    const cardList = (event) => {
        setCard({ id: new Date().getTime().toString(), name: event.target.value })

    }

    // ADD CARD
    let temp = props.temp
    
    const AddCard = () => {
        let key = 'ca306ee3bb37dcc0aefd448047b05d63'
        let token = 'ab25220c61d4bfadce681521aa57499eea73f31867f52b7227737eadbd7af39d'

        if (card.name == '') return

        setCardItem((oldList) => {
            let object = { id: card.id, name: card.name }
            return ([...oldList, object])
        })
        fetch(`https://api.trello.com/1/cards?name=${card.name}&idList=${temp[props.name]}&key=${key}&token=${token}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json'
            }
        })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
            .then(text => {
                let cardObj = JSON.parse(text)
                cardId[card.name] = cardObj.id

                setCardId(cardObj.id)
            })
            .catch(err => console.error(err));

    }

    useEffect(()=>{
        async function getCard(){
            let URL=await fetch(`https://api.trello.com/1/lists/${temp[props.name]}/cards?key=${key}&token=${token}`, {
                method:'GET'
            })
            return URL.json()
        }
        getCard().then((element)=>{
            let cardsArray=[]
            element.forEach((value)=>{
                cardsArray.push({'id':value.id,'name':value.name})
                cardId[value.name]=value.id
            })
            setCardItem(cardsArray)

        })
    },[])



    return (
        <div className='list-container'>

        <div className='main'>
            <div className='trelloBoard'>
                <h3 className='title'>{props.name}</h3>
                <div className='cardInfo'>
                    <input type='text' onChange={cardList} className='cardInput'></input>
                    <button onClick={AddCard} className='grow'>Add</button>
                    <button onClick={() => props.onSelect(props.id)} className='grow'>Delete</button>
                </div>


                <ol className='lists-Container'>
                    {cardItem.map((element) => {
                        return <TrelloCard name={element.name} cardId={cardId} card_id={card_id} temp={props.temp}/>
                    })}
                </ol>               
            </div>
        </div>
        </div>
    )
}
export default TrelloLists
