import React, { useEffect, useState } from 'react'

let key = 'ca306ee3bb37dcc0aefd448047b05d63'
let token = 'ab25220c61d4bfadce681521aa57499eea73f31867f52b7227737eadbd7af39d'
const CheckListItem = (props) => {
    
    const [isComplete, setIsComplete] = useState(false)
    const [checkItemStrike,setCheckItemStrike]=useState(true)
    const [complete,setIncomplete]=useState('Incomplete')

    
    
    let id=props.id
    let title=props.title
    let checkLists=props.checkLists
    let checkItem=props.checkItemId
    
    let cardIdObject=props.cardId
    let cardName= props.cardName

    const completeCheckList = () => {
        !isComplete ? setIsComplete(true) : setIsComplete(false)

        // CHECK ITEM COMPLETE OR UN COMPLETE
        !checkItemStrike?setCheckItemStrike(true):setCheckItemStrike(false)
        checkItemStrike?setIncomplete('Incomplete'):setIncomplete('complete')
        
        
        // UPDATING CHECK ITEM COMPLETE OR INCOMPLETE
        fetch(`https://api.trello.com/1/cards/${cardIdObject[cardName]}/checklist/${checkLists[title]}/checkItem/${checkItem[props.value]}?state=${checkItemStrike}&key=${key}&token=${token}`, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json'
            }
        })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
            .then(text => console.log(text))
            .catch(err => console.error(err));
    }
    // AFTER REFRESH CHECK ITEM COMPLETE OR INCOMPLETE
    useEffect(()=>{
        async function getStatus(){
            let URL = await fetch(`https://api.trello.com/1/cards/${cardIdObject[cardName]}/checkItem/${checkItem[props.value]}?key=${key}&token=${token}`,{
                method:'GET'
            })
            return URL.json()
        }
        getStatus().then((item)=>{
            console.log(item.state)
            setIncomplete(item.state)
            if(item.state=='complete'){
                setCheckItemStrike(false)
                setIsComplete(true)
            }
        })
    },[])
    
    return (
        <>
            <div className='checkItem'>
                <i className="fa-solid fa-square-check" onClick={completeCheckList}></i>
                <li style={{ textDecoration: isComplete ? "line-through" : "none" }}>{props.value}</li>
            </div>
        </>
    )
}

export default CheckListItem