import React, { useEffect, useState } from 'react'
import TrelloLists from './TrelloList'
import GetList from './GetList'
import './index.css'
import 'tachyons'


let temp = {}
const ListAdd = () => {
    const [inputVal, setInputValue] = useState({ id: "", name: "" })
    const [item, setItem] = useState([])

    let boardId = '623afa747f70097e37db6797'
    let key = 'ca306ee3bb37dcc0aefd448047b05d63'
    let token = 'ab25220c61d4bfadce681521aa57499eea73f31867f52b7227737eadbd7af39d'


    const TrelloList = (event) => {
        setInputValue({ id: new Date().getTime().toString(), name: event.target.value })
    }


    // ADD LIST
    const AddList = () => {
        if (inputVal.name == "") return
        setItem((oldList) => {
            let object = { id: inputVal.id, name: inputVal.name }
            return ([...oldList, object])
        })

        // POSTING LIST
        fetch(`https://api.trello.com/1/lists?name=${inputVal.name}&idBoard=623afa747f70097e37db6797&key=${key}&token=${token}`, {
            method: 'POST'
        })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
            .then(text => {
                let tempObject = JSON.parse(text)
                let objectId = tempObject['id']
                temp[inputVal.name] = objectId

            })
            .catch(err => console.error(err));


    }


    // DELETE LIST
    const deleteList = (id) => {
        setItem((item.filter((lists) => lists.id != id)))


        // DELETE LIST FROM TRELLO BOARD
        fetch(`https://api.trello.com/1/lists/${temp[inputVal.name]}/closed?value=true&key=${key}&token=${token}`, {
            method: 'PUT'
        })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
            .then(text => console.log(text))
            .catch(err => console.error(err));
    }

    return (
        <>
            <div className='add_list'>
                <h1>Trello Board</h1>
                <input type="text" value={inputVal.name} className='input' onChange={TrelloList} placeholder='Add a list...'></input>
                <button onClick={AddList} className='grow'>Add</button>
            </div>


            <ol className='lists'>
                {item.map((listItem) => {

                    return <TrelloLists key={listItem.id} id={listItem.id} name={listItem.name} temp={temp} onSelect={deleteList} />
                })}
            </ol>

            <ol className='listContainer'>
                <GetList />
            </ol>

        </>
    )
}

export default ListAdd